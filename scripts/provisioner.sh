#!/bin/bash

echo -e "\n\tEnable EPEL repos\n"
sudo dnf update -y
sudo dnf install dnf-plugins-core -y
sudo dnf config-manager --set-enabled crb -y
sudo dnf install epel-release epel-next-release -y
sudo dnf update --refresh

echo -e "\n\tEnable sshd\n"
sudo systemctl enable sshd

echo -e "\n\tInstall tools\n"
sudo dnf install bzip2 lzop unzip wget npm openssl-devel bash-completion gcc lldb git gdb g++ clang llvm cmake neovim podman buildah skopeo -y
mkdir -p /home/ec2-user/.local/{opt,bin}

echo -e "\n\tInstall aws cli\n"
curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip'
unzip awscliv2.zip
rm awscliv2.zip
./aws/install -i /home/ec2-user/.local/opt/aws-cli -b /home/ec2-user/.local/bin
rm -rf ./aws

echo -e "\n\tClean\n"
echo -e "shopt -s globstar\nshopt -s autocd\nshopt -s histappend\nshopt -s extglob\nHISTCONTROL=ignoredups:erasedups" >> /home/ec2-user/.bashrc
echo "export PATH=/home/ec2-user/.local/bin:${PATH}" >> /home/ec2-user/.bashrc
echo "[[ -f /etc/profile.d/bash_completion.sh ]] && source /etc/profile.d/bash_completion.sh" >> /home/ec2-user/.bashrc
echo "[[ $(which aws_completer 2> /dev/null) ]] && complete -C $(which aws_completer) aws" >> /home/ec2-user/.bashrc

echo -e "\n\tEnable sshd\n"
sudo dnf clean all
sudo rm -rf /var/cache /var/log/dnf* /var/log/yum.*


