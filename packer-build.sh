#!/bin/bash

packages=( packer )

base_ami=""
ami_name=""
ami_manifest=""

RED='\033[1;31m'
BLUE='\033[1;36m'
NOCOLOR='\033[0;0m'

function Welcome() {
  echo -e "\n\tPacker build wrapper\n\n\t${BLUE}START${NOCOLOR}\n\n"
}

function EnsureDependenciesInstalled() {
  errors=()
  for i in ${!packages[@]}; do
    [[ ! $(whereis ${packages[i]} | cut -d':' -f2 | wc -w) -gt 0 ]] && errors+=("${packages[i]} not installed")
  done

  if [[ ${#errors[@]} -gt 0 ]]; then
    for i in ${!errors[@]}; do
      printf "${RED}${errors[$i]}${NOCOLOR}\n"
    done
    exit 1 
  fi
}

function PackerBuild() {
  set -a
  . .env
  set +a

  output=$(packer build \
        -machine-readable \
        -var "ami_name=${PACKER_AMI_NAME}" \
        -var "base_ami=${PACKER_BASE_AMI}" \
        -var "vpc_id=${PACKER_VPC_ID}" \
        -var "subnet_id=${PACKER_SUBNET_ID}" \
        -var "ssh_username=${PACKER_SSH_USERNAME}" \
        -var "instance_size=${PACKER_INSTANCE_SIZE}" \
        -var "description=${PACKER_DESCRIPTION}" \
        -var "manifest=${PACKER_MANIFEST_NAME}" \
        ${PACKER_CONTEXT} 2>&1 | tee /dev/tty)

  if [[ ! $? -eq 0 ]]; then
     [[ $(echo ${output} | grep "Error") = *"existing"*"AMI"* ]] && echo -e "\n\t${BLUE}Image ${ami_name} already exists - skipping${NOCOLOR}\n" || return $?
  fi
}

function GoodBye() {
  echo -e "\n\n\t${BLUE}END${NOCOLOR}\n\n"
}

Welcome
EnsureDependenciesInstalled
PackerBuild
GoodBye





