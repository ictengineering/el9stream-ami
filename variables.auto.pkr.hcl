variable "ami_name" {
  type    = string
  default = ""
}

variable "base_ami" {
  type    = string
  default = "ami-032b34c31d6a07b87"
}

variable "description" {
  type    = string
  default = ""
}

variable "ssh_username" {
  type    = string
  default = ""
}

variable "subnet_id" {
  type    = string
  default = ""
}

variable "vpc_id" {
  type    = string
  default = ""
}

variable "instance_size" {
  type = string
  default = "t2.micro"
}

variable "script" {
  type = string
  default = ""
}

variable "manifest" {
  type = string
  default = ""
}
