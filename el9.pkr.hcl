
source "amazon-ebs" "el9stream" {
  ami_name      = "${var.ami_name}"
  instance_type = "${var.instance_size}"
  region        = "eu-west-1"
  source_ami    = "${var.base_ami}"
  ssh_pty       = "true"
  ssh_timeout   = "20m"
  ssh_username  = "${var.ssh_username}"
  subnet_id     = "${var.subnet_id}"
  tags = {
    BuiltBy = "ms with packer"
    Name    = "centos stream 9"
  }
  vpc_id = "${var.vpc_id}"
}

build {
  description = "${var.description}"

  sources = ["source.amazon-ebs.el9stream"]

  provisioner "shell" {
    scripts = [
      "scripts/provisioner.sh"
    ]
  }

  post-processors {
    post-processor "manifest" {
      output = "${var.manifest}"
      strip_path = true
    }
  }
}


